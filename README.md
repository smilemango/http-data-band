# http-data-band

http-data-band is a simple TCP tunnel, transported over HTTP. 
There is a HTTP Client executable(htc) and a HTTP Server executable(hts). 
Written in python. 
http-data-band is mainly useful for passing through firewalls which could detect http protocol.


## Install sources

**Prerequirement**

* Python 3.5
* PyQT 5.6 for Python 3.5
* (optional) pyinstaller

**Configuration**

ht.ini
```
[HTTP_CLIENT]  <---- information for http tunnel client 
IP=127.0.0.1
PORT=3390

[HTTP_SERVER]  <---- information for http tunnel server
IP=127.0.0.1
PORT=80

[DEST_SERVER]  <---- information for last destination server
IP=127.0.0.1
PORT=443
```


**Run**

* First, run hts.py
```
$ python hts.py
```

* Second, run htc.py
```
$ python htc.py
```

* Third, run your own server and client



### Known Issues

* Transfer via SSL Layer
* GUI for easy usage
* Improve performance


### Changelog

* `0.0.1` - Init


#### MIT License

Copyright © 2015 Jaime Pillora &lt;dev@jpillora.com&gt;

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
'Software'), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.