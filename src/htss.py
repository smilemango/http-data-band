import socketserver
import configparser

class MyTCPHandler(socketserver.BaseRequestHandler):
    """
    The RequestHandler class for our server.

    It is instantiated once per connection to the server, and must
    override the handle() method to implement communication to the
    client.
    """

    def handle(self):
        print("connected from {}".format(self.client_address[0]))
        while True:
            # self.request is the TCP socket connected to the client
            self.data = self.request.recv(1024)
            if len( self.data) == 0 :
                print("disconnected by the peer.")
                break
            print("{} wrote:".format(self.client_address[0]))
            print(self.data)
            # just send back the same data, but upper-cased
            self.request.sendall(self.data.upper())

if __name__ == "__main__":

    config = configparser.ConfigParser()
    config.read('ht-test.ini')


    HOST, PORT = config['TEST_SERVER']['IP'], int(config['TEST_SERVER']['PORT'])
    print("Server Start")
    # Create the server, binding to localhost on port 9999
    server = socketserver.TCPServer((HOST, PORT), MyTCPHandler)

    # Activate the server; this will keep running until you
    # interrupt the program with Ctrl-C
    server.serve_forever()
