import base64
import cgi
import configparser
import logging
import select
import socket
import socketserver
import threading
from http.server import BaseHTTPRequestHandler,HTTPServer

IS_RUNNING = False
TUNNEL_THREAD = None


class MyHandler(BaseHTTPRequestHandler):

    sockets = {}
    BUFFER = 1024 * 50
    SOCKET_TIMEOUT = 50

    def _get_connection_id(self):
        return self.path.split('/')[-1]

    def _get_socket(self):
        """get the socket which connects to the target address for this connection"""
        id = self._get_connection_id()
        return self.sockets.get(id, None)

    def _close_socket(self):
        """ close the current socket"""
        id = self._get_connection_id()
        try:
            s = self.sockets.get(id,None)

            if s:
                s.close()
                logging.debug("DELETE SOCKET by '%s'" % id)
                del self.sockets[id]
        except:
            logging.warning("SOCKET_LIST:%s" % self.sockets)

    def do_POST(self):
        """POST: Create TCP Connection to the TargetAddress"""
        id = self._get_connection_id()
        logging.info(  'Initializing connection with ID %s' % id)
        length = int(self.headers.get('content-length'))
        logging.debug( "Request Header:\n%s" % self.headers)
        logging.debug( "Request Line:\n%s" % self.requestline)
        logging.debug( 'Content-Length: %s' %length)
        req_data = self.rfile.read(length)
        logging.debug( 'Request Data:\n %s' %  req_data)
        params = cgi.parse_qs(req_data, keep_blank_values=1)
        logging.debug( 'Parsed Param:\n %s' %  params)
        target_host = params[b'host'][0]
        target_port = int(params[b'port'][0])

        logging.info('Connecting to target address: %s % s' % (target_host, target_port))
        # open socket connection to remote server
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        # use non-blocking socket
        s.setblocking(0)
        s.connect_ex((target_host, target_port))

        #save socket reference
        self.sockets[id] = s
        try:
            self.send_response(200)
            self.end_headers()
        except socket.error as e:
            logging.error(  e)

    def do_GET(self):
        """GET: Read data from TargetAddress and return to client through http response"""
        logging.info(  "Wating for data from SERVER")
        s = self._get_socket()
        if s:
            # check if the socket is ready to be read
            to_reads, to_writes, in_errors = select.select([s], [], [])
            to_read_socket = to_reads[0]
            try:
                logging.debug("to read : %s" % to_reads)
                logging.info( "Getting data from target address")
                data = to_read_socket.recv(self.BUFFER)
                if len(data) > 0 :
                    logging.debug( "Data from target address : [%s]" % data)
                    self.send_response(200)
                    self.end_headers()
                    if data:
                        self.wfile.write(data)
                else:
                    logging.info(  'No content available from socket')
                    self.send_response(404) # no content had be retrieved
                    self.end_headers()
            except socket.error as ex:
                logging.error(  'Error getting data from target socket: %s' % ex )
                self.send_response(503)
                self.end_headers()
        else:
            logging.error( 'Connection With ID %s has not been established' % self._get_connection_id())
            self.send_response(400)
            self.end_headers()
        logging.info(  "GET Relay completed.")




    def do_PUT(self):
        """Read data from HTTP Request and send to TargetAddress"""
        logging.info(  "Wating for data from htc")
        id = self._get_connection_id()
        s = self.sockets[id]
        if not s:
            logging.info(  "Connection with id %s doesn't exist" % id)
            self.send_response(400)
            self.end_headers()
            return
        length = int(self.headers.get('content-length'))
        b64_data = cgi.parse_qs(self.rfile.read(length), keep_blank_values=1)[b'data'][0]
        data = base64.b64decode( b64_data)

        # check if the socket is ready to write
        to_reads, to_writes, in_errors = select.select([], [s], [], 5)
        logging.info(  "Data from htc(size:%s)" % len(to_writes))
        if len(to_writes) > 0:
            logging.debug(  'Sending data .... %s' % data)
            to_write_socket = to_writes[0]
            try:
                to_write_socket.sendall(data)
                self.send_response(200)
            except socket.error as ex:
                logging.error( 'Error sending data from target socket: %s' % ex)
                self.send_response(503)
        else:
            logging.error( 'Socket is not ready to write')
            self.send_response(504)
        self.end_headers()
        logging.info(  'PUT Relay completd.')

    def do_DELETE(self):
        self._close_socket()
        self.send_response(200)
        self.end_headers()

class ThreadedHTTPServer(socketserver.ThreadingMixIn,HTTPServer):
    pass


def start_tunnel(port):
    global TUNNEL_THREAD
    global httpd
    global IS_RUNNING

    httpd = ThreadedHTTPServer(("", port), MyHandler)
    logging.info('Starting Http Tunneling Server - Port:%s' % port)

    TUNNEL_THREAD = threading.Thread(target=httpd.serve_forever)
    TUNNEL_THREAD.start()

    IS_RUNNING = True


def stop_tunnel():
    global httpd
    global IS_RUNNING
    httpd.shutdown()
    IS_RUNNING = False

if __name__ == '__main__':
    #logging.basicConfig(filename='hts.log',format='%(asctime)s [%(levelname)-5s] %(message)s',level=logging.DEBUG)
    logging.basicConfig(format='%(asctime)s %(threadName)s [%(levelname)-5s] %(message)s',level=logging.INFO)

    config = configparser.ConfigParser()
    config.read('ht.ini')

    SVR_HOST, SVR_PORT = config['HTTP_SERVER']['IP'], int(config['HTTP_SERVER']['PORT'])

    httpd = ThreadedHTTPServer((SVR_HOST, SVR_PORT), MyHandler)
    logging.info('Starting Http Tunneling Server - %s:%s' % (SVR_HOST, SVR_PORT))

    try:
        httpd.serve_forever()
    except KeyboardInterrupt:
        logging.info('Server Stopping ')
        pass

    httpd.server_close()
    logging.info('Server Stops - %s:%s' % (SVR_HOST, SVR_PORT))
