# -*- mode: python -*-

block_cipher = None


a = Analysis(['ht_cp.py'],
             pathex=['.'],
             binaries=[],
             datas=[('image/*','image'),('ui/*','ui'),('ht.ini','.')],
             hiddenimports=[],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          exclude_binaries=True,
          name='http-data-band',
          debug=False,
          strip=False,
          upx=True,
          console=False,
          icon='.\\image\\mango tunnel.ico'
          )
coll = COLLECT(exe,
               a.binaries,
               a.zipfiles,
               a.datas,
               strip=False,
               upx=True,
               name='http-data-band')
