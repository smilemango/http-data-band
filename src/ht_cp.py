import configparser
import ctypes
import logging
import os
import sys
import threading
import time

import htc
import hts
from PyQt5 import QtCore
from PyQt5 import uic
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *


def get_running_path():
    # if running by exe
    # ref : https://pythonhosted.org/PyInstaller/runtime-information.html
    running_path = ""
    if getattr(sys, 'frozen', False):
        running_path = os.path.dirname(sys.executable)
    # if running by script
    else:
        running_path = os.getcwd()

    return running_path


RUNNING_PATH = get_running_path()
ControlPanelDialog, ControlPanelBase = uic.loadUiType("%s/ui/ht-cp.ui" % RUNNING_PATH)

myappid = 'net.engear.http-data-band.0.0.2'  # arbitrary string
ctypes.windll.shell32.SetCurrentProcessExplicitAppUserModelID(myappid)


class MyTrayIcon(QSystemTrayIcon):
    def __init__(self, icon, parent=None):
        QSystemTrayIcon.__init__(self, icon, parent)

        menu = QMenu(parent)
        openAction = menu.addAction("Open")
        openAction.triggered.connect(parent.openDialogEvent)

        exitAction = menu.addAction("Exit")
        exitAction.triggered.connect(parent.exitEvent)

        self.setContextMenu(menu)


class ControlPanelMainForm(QDialog, ControlPanelDialog):
    def __init__(self, my_parent, config):
        super().__init__(parent=my_parent)
        self.setupUi(self)
        self.setWindowIcon(QIcon(RUNNING_PATH + '/image/mango tunnel.ico'))

        self.btnClntStart.clicked.connect(self.btnClntStart_Clicked)
        self.btnSvrStart.clicked.connect(self.btnSvrStart_Clicked)
        self.rbClient.clicked.connect(self.rbClient_Clicked)
        self.rbServer.clicked.connect(self.rbServer_Clicked)

        self.config = config

    def init_data(self):
        self.lbl_clnt_port.setText(self.config['HTTP_CLIENT']['PORT'])
        self.lbl_hts_addr.setText("%s:%s" % (self.config['HTTP_SERVER']['IP'], self.config['HTTP_SERVER']['PORT']))
        self.lbl_dest_addr.setText("%s:%s" % (self.config['DEST_SERVER']['IP'], self.config['DEST_SERVER']['PORT']))
        self.lbl_loglevel.setText(self.config['GENERAL']['LOGLEVEL'])
        self.lbl_svr_port.setText(self.config['HTTP_SERVER']['PORT'])

        if self.config['GENERAL']['MODE'] == 'CLIENT':
            self.rbClient.setChecked(True)
            self.rbServer.setChecked(False)
            self.gboxClnt.setEnabled(True)
            self.gboxSvr.setEnabled(False)
        else:
            self.rbClient.setChecked(False)
            self.rbServer.setChecked(True)
            self.gboxClnt.setEnabled(False)
            self.gboxSvr.setEnabled(True)

    def rbClient_Clicked(self):
        self.config['GENERAL']['MODE'] = 'CLIENT'
        self.init_data()
        write_config(self.config)

    def rbServer_Clicked(self):
        self.config['GENERAL']['MODE'] = 'SERVER'
        self.init_data()
        write_config(self.config)

    def btnClntStart_setText(self, text):
        self.btnClntStart.setText(text)

    def btnSvrStart_setText(self, text):
        self.btnSvrStart.setText(text)

    def btnClntStart_Clicked(self):
        if not htc.IS_RUNNING:
            logging.info('Starting...Client Tunnel.')
            logging.info("Log level is '%s'" % strLogLevel)

            HOST, PORT = config['HTTP_CLIENT']['IP'], int(config['HTTP_CLIENT']['PORT'])
            SVR_HOST, SVR_PORT = config['HTTP_SERVER']['IP'], int(config['HTTP_SERVER']['PORT'])
            DEST_HOST, DEST_PORT = config['DEST_SERVER']['IP'], int(config['DEST_SERVER']['PORT'])

            remote_addr = {"host": SVR_HOST, "port": SVR_PORT}
            target_addr = {"host": DEST_HOST, "port": DEST_PORT}

            htc.start_tunnel(PORT, remote_addr, target_addr)
        else:
            logging.info('Stopping...Client Tunnel.')
            htc.stop_tunnel()

    def btnSvrStart_Clicked(self):
        if not hts.IS_RUNNING:
            logging.info('Starting...Server Tunnel.')
            logging.info("Log level is '%s'" % strLogLevel)

            SVR_HOST, SVR_PORT = config['HTTP_SERVER']['IP'], int(config['HTTP_SERVER']['PORT'])

            hts.start_tunnel(SVR_PORT)
        else:
            logging.info('Stopping...Client Tunnel.')
            hts.stop_tunnel()

    def closeEvent(self, event):
        event.ignore()
        self.setWindowState(QtCore.Qt.WindowMinimized)
        print("event")

    def exitEvent(self):
        if htc.IS_RUNNING:
            logging.info('Stopping...Client Tunnel.')
            htc.stop_tunnel()

        if hts.IS_RUNNING:
            logging.info('Stopping...Client Tunnel.')
            hts.stop_tunnel()

        super(ControlPanelMainForm, self).closeEvent(None)
        print("exit")

    def openDialogEvent(self):
        self.setWindowState(QtCore.Qt.WindowActive)


def check_tunnel_state_and_update(w):
    while IS_RUNNING:
        if htc.IS_RUNNING:
            w.btnClntStart_setText("Stop")
        else:
            w.btnClntStart_setText("Start")

        if hts.IS_RUNNING:
            w.btnSvrStart_setText("Stop")
        else:
            w.btnSvrStart_setText("Start")

        time.sleep(1)


def write_config(config):
    global RUNNING_PATH

    fp = open(RUNNING_PATH + '/ht.ini', 'w')
    config.write(fp)
    fp.close()

    logging.info("Config file updated.")


if __name__ == '__main__':

    config = configparser.ConfigParser()
    config.read(RUNNING_PATH + '/ht.ini')

    strLogLevel = config['GENERAL']['LOGLEVEL']
    if strLogLevel == 'DEBUG':
        logLevel = logging.DEBUG
    elif strLogLevel == 'ERROR':
        logLevel = logging.ERROR
    elif strLogLevel == 'WARN':
        logLevel = logging.WARN
    else:
        logLevel = logging.INFO

    try:
        print("Running path+logfile:%s" % RUNNING_PATH + '/log/http-data-band.log')
        logging.basicConfig(filename=RUNNING_PATH + '/log/http-data-band.log',
                            format='%(asctime)s  %(threadName)s [%(levelname)-5s] %(message)s',
                            level=logLevel)
    except FileNotFoundError:
        os.mkdir(RUNNING_PATH + '/log')
        logging.basicConfig(filename=RUNNING_PATH + '/log/http-data-band.log',
                            format='%(asctime)s [%(levelname)-5s] %(message)s',
                            level=logLevel)

    logging.info('Starting...')
    logging.info("Log level is '%s'" % strLogLevel)

    strLogLevel = config['GENERAL']['LOGLEVEL']
    if strLogLevel == 'DEBUG':
        logLevel = logging.DEBUG
    elif strLogLevel == 'ERROR':
        logLevel = logging.ERROR
    elif strLogLevel == 'WARN':
        logLevel = logging.WARN
    else:
        logLevel = logging.INFO

    app = QApplication(sys.argv)
    widget = QWidget()
    w = ControlPanelMainForm(widget, config)
    trayIcon = MyTrayIcon(QIcon(RUNNING_PATH + "/image/mango tunnel.png"), w)
    trayIcon.show()

    w.init_data()
    w.show()

    IS_RUNNING = True

    THREAD_CHECK_CLIENT = threading.Thread(target=check_tunnel_state_and_update, args=(w,))
    THREAD_CHECK_CLIENT.start()

    app.exec_()

    IS_RUNNING = False
    logging.info("Waiting for stop.")
    THREAD_CHECK_CLIENT.join()
    logging.info("SERVICE is terminated.")
    trayIcon.hide()
