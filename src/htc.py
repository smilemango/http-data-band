import base64
import configparser
import http.client
import logging
import socket
import sys
import threading
import urllib.parse
from uuid import uuid4

BUFFER = 1024 * 50

IS_RUNNING = False
TUNNEL_THREAD = None
LISTEN_SOCK = None

class ConnectionClosedException(Exception):
    pass


class Connection():
    def __init__(self, connection_id, remote_addr):
        self.id = connection_id
        conn_dest = remote_addr
        logging.info("Establishing connection with remote tunneld at %s:%s" % (conn_dest['host'], conn_dest['port']))
        self.http_conn = http.client.HTTPConnection(conn_dest['host'], conn_dest['port'])
        self.remote_addr = remote_addr

    def _url(self, url):
        return "http://{host}:{port}{url}".format(host=self.remote_addr['host'], port=self.remote_addr['port'], url=url)

    def create(self, target_addr):
        params = urllib.parse.urlencode({"host": target_addr['host'], "port": target_addr['port']})
        headers = {"Content-Type": "application/x-www-form-urlencoded", "Accept": "text/plain"}

        self.http_conn.request("POST", self._url("/" + self.id), params, headers)

        response = self.http_conn.getresponse()
        response.read()
        if response.status == 200:
            logging.info('Successfully create connection')
            return True
        else:
            logging.info('Fail to establish connection: status %s because %s' % (response.status, response.reason))
            return False

    def send(self, data):
        params = urllib.parse.urlencode({"data": base64.b64encode(data)})

        headers = {"Content-Type": "application/x-www-form-urlencoded", "Accept": "text/plain"}
        try:
            self.http_conn.request("PUT", self._url("/" + self.id), params, headers)
            response = self.http_conn.getresponse()
            response.read()
            logging.debug("RESPONSE: %s " % response.status)
        except (http.client.HTTPResponse, socket.error) as ex:
            logging.info("Error Sending Data: %s" % ex)

    def receive(self):
        try:
            self.http_conn.request("GET", "/" + self.id)
            response = self.http_conn.getresponse()
            data = response.read()
            if response.status == 200:
                return data
            elif response.status == 404:
                raise ConnectionClosedException
            else:
                return None
        except ConnectionClosedException:
            raise ConnectionClosedException
        except ConnectionResetError:
            logging.error("Socket@HttpClientTunnel is already closed.")
            raise ConnectionClosedException
        except (http.client.HTTPResponse, socket.error) as ex:
            logging.error("Error Receiving Data: %s" % ex)
            return None

    def close(self):
        logging.info("Close connection to target at remote tunnel")
        self.http_conn.request("DELETE", "/" + self.id)
        self.http_conn.getresponse()


class SendThread(threading.Thread):
    """
    Thread to send data to remote host
    """

    def __init__(self, client, connection):
        threading.Thread.__init__(self, name="Send-Thread@htc")
        self.client = client
        self.socket = client.socket
        self.conn = connection
        self._stop = threading.Event()

    def run(self):
        while not self.stopped():
            logging.info("Getting data from client to send")
            logging.debug("Socket : %s " % self.socket)
            try:
                data = self.socket.recv(BUFFER)
                if data == b'':
                    logging.info("Client's socket connection broken")
                    # There should be a nicer way to stop receiver
                    # self.client.receiver.stop()
                    # self.client.receiver.join()
                    self.conn.close()
                    return

                logging.debug("Sending data ... %s " % data)
                self.conn.send(data)
            except socket.timeout:
                logging.info("time out")
            except IOError:
                logging.warning("SendThread Socket was already closed.")

    def stop(self):
        if not self.stopped():
            logging.debug("SendThread is stopping.")
            self._stop.set()

    def stopped(self):
        return self._stop.isSet()


class ReceiveThread(threading.Thread):
    """
    Thread to receive data from remote host
    """

    def __init__(self, client, connection):
        threading.Thread.__init__(self, name="Receive-Thread@htc")
        self.client = client
        self.socket = client.socket
        self.conn = connection
        self._stop = threading.Event()

    def run(self):
        while not self.stopped():
            logging.info("Retreiving data from remote tunneld")
            # logging.debug( "Socket : %s " % self.socket )
            try:
                data = self.conn.receive()
                if data:
                    sent = self.socket.sendall(data)
                else:
                    raise ConnectionClosedException
            except ConnectionClosedException:
                logging.info("Connection Closed by Server")
                self.stop()

    def stop(self):

        if not self.stopped():
            logging.debug("ReceiveThread is stopping.")
            self._stop.set()
            self.client.stop()

    def stopped(self):
        return self._stop.isSet()


class ClientWorker(object):
    def __init__(self, socket, remote_addr, target_addr):
        self.socket = socket
        self.remote_addr = remote_addr
        self.target_addr = target_addr

    def start(self):
        # generate unique connection ID
        connection_id = str(uuid4())
        # main connection for create and close
        self.connection = Connection(connection_id, self.remote_addr)

        if self.connection.create(self.target_addr):
            self.sender = SendThread(self, Connection(connection_id, self.remote_addr))
            self.receiver = ReceiveThread(self, Connection(connection_id, self.remote_addr))
            self.sender.start()
            self.receiver.start()

    def stop(self):
        # stop read and send threads
        self.sender.stop()
        self.receiver.stop()
        # send close signal to remote server
        logging.debug("ClientSocket is closing.")
        self.connection.close()
        # wait for read and send threads to stop and close local socket
        # self.sender.join()
        # self.receiver.join()
        self.socket.close()


def __tunnel_running_thread(listen_port, remote_addr, target_addr):
    logging.info('Starting Http Tunneling Client - Port:%s' % listen_port)

    global LISTEN_SOCK
    global IS_RUNNING
    LISTEN_SOCK = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    LISTEN_SOCK.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    LISTEN_SOCK.settimeout(None)
    LISTEN_SOCK.bind(('', int(listen_port)))
    LISTEN_SOCK.listen(1)
    workers = []
    try:
        IS_RUNNING = True
        while IS_RUNNING:
            c_sock, addr = LISTEN_SOCK.accept()
            c_sock.settimeout(20)
            logging.info("Connected by %s" % addr[0])
            worker = ClientWorker(c_sock, remote_addr, target_addr)
            workers.append(worker)
            worker.start()
    except (KeyboardInterrupt, SystemExit):
        LISTEN_SOCK.close()
        for w in workers:
            w.stop()
        # for w in workers:
        #    w.join()
        logging.info('Server Stops - %s:%s' % (HOST, PORT))
        sys.exit()
    except:
        logging.info("Tunneling Client is just Stopped.")


def stop_tunnel():
    global IS_RUNNING
    global LISTEN_SOCK
    if LISTEN_SOCK:
        LISTEN_SOCK.close()
        LISTEN_SOCK = None

    IS_RUNNING = False



def start_tunnel(listen_port, remote_addr, target_addr, is_threading=True):
    """Start tunnel"""
    global TUNNEL_THREAD

    if is_threading:

        TUNNEL_THREAD = threading.Thread(target=__tunnel_running_thread, name="MainThrad@htc",
                                         args=(listen_port, remote_addr, target_addr))
        TUNNEL_THREAD.start()

    else:
        __tunnel_running_thread(listen_port, remote_addr, target_addr)


if __name__ == '__main__':

    config = configparser.ConfigParser()
    config.read('ht.ini')

    strLogLevel = config['LOG']['LEVEL']
    if strLogLevel == 'DEBUG':
        logLevel = logging.DEBUG
    elif strLogLevel == 'ERROR':
        logLevel = logging.ERROR
    elif strLogLevel == 'WARN':
        logLevel = logging.WARN
    else:
        logLevel = logging.INFO

    logging.basicConfig(filename='./log/htc.log', format='%(asctime)s  %(threadName)s [%(levelname)-5s] %(message)s',
                        level=logLevel)
    logging.info('Starting...')
    logging.info("Log level is '%s'" % strLogLevel)

    HOST, PORT = config['HTTP_CLIENT']['IP'], int(config['HTTP_CLIENT']['PORT'])
    SVR_HOST, SVR_PORT = config['HTTP_SERVER']['IP'], int(config['HTTP_SERVER']['PORT'])
    DEST_HOST, DEST_PORT = config['DEST_SERVER']['IP'], int(config['DEST_SERVER']['PORT'])

    remote_addr = {"host": SVR_HOST, "port": SVR_PORT}
    target_addr = {"host": DEST_HOST, "port": DEST_PORT}

    start_tunnel(PORT, remote_addr, target_addr)
