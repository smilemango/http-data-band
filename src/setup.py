import sys
from cx_Freeze import setup, Executable

setup(  name = "mango tunnel",
        version = "0.0.1",
        description = "mango tunnel",
        author = "smilemango",
        executables = [Executable("ht_cp.py")])