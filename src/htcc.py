import socket
import sys
import configparser

config = configparser.ConfigParser()
config.read('ht.ini')


HOST, PORT = config['HTTP_CLIENT']['IP'], int(config['HTTP_CLIENT']['PORT'])

#data = " ".join(sys.argv[1:])

# Create a socket (SOCK_STREAM means a TCP socket)
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

try:
    # Connect to server and send data
    sock.connect((HOST, PORT))

    try:
        while True:
            data = input("입력:")
            if data == 'exit':
                break
            sock.sendall(bytes(data + "\n", "utf-8"))
            # Receive data from the server and shut down
            received = str(sock.recv(1024), "utf-8")
            print("수신:{}".format(received))
    except KeyboardInterrupt:
        print("\n종료됨")
        pass

finally:
    sock.close()
